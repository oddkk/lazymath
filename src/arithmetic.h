#ifndef ARITHMETIC_H
#define ARITHMETIC_H

#include <map>
#include <stack>
#include <vector>
#include <string>
#include <memory>
#include "token.h"

class Function;
struct State {
	std::vector<std::map<std::string, Function>> functions;
	std::stack<double> stack;
};

class ArithExpression {
public:
	virtual void eval(State& s)=0;
};

class ArithSequence : ArithExpression {
public:
	virtual void eval(State& s);
private:
	std::vector<std::unique_ptr<ArithExpression>> expr;
};

class ArithOperator : public ArithExpression {
public:
	virtual void eval(State& s);
private:
	Operator value;
};

class ArithConstant : public ArithExpression {
public:
	virtual void eval(State& s);
private:
	double value;
};

class ArithFunction : public ArithExpression {
public:
	virtual void eval(State& s);
private:
	std::string value;
	std::vector<ArithSequence> params;
};

class Equation {
public:
	ArithSequence lhs;
	ArithSequence rhs;
};

class Function {
public:
	void eval(std::vector<ArithSequence> params, State& s);
	std::string name;
	std::vector<std::string> variables;
	ArithSequence seq;
};

#endif
