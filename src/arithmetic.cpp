#include "arithmetic.h"
#include <math.h>
#include <stdexcept>

void ArithSequence::eval(State& s) {
	for (unsigned int i = 0; i < expr.size(); i++) {
		expr[i]->eval(s);
	}
}

void ArithOperator::eval(State& s) {
	double d1 = s.stack.top(); s.stack.pop();
	double d2 = s.stack.top(); s.stack.pop();
	double r = 0.0;
	switch (value) {
	case Operator::plus: r = d1 + d2; break;
	case Operator::minus: r = d1 - d2; break;
	case Operator::multiply: r = d1 * d2; break;
	case Operator::divide: r = d1 / d2; break;
	case Operator::power: r = pow(d1, d2); break;
	default: break;
	}
	s.stack.push(r);
}

void ArithConstant::eval(State& s) {
	s.stack.push(value);
}

void ArithFunction::eval(State& s) {
	for (auto it = s.functions.rbegin(); it != s.functions.rend(); it++) {
		auto f = it->find(value);
		if (f != it->end()) {
			f->second.eval(params, s);
			return;
		}
	}
	throw std::runtime_error("Function " + value + " does not exist");
}

void Function::eval(std::vector<ArithSequence> params, State& s) {
	if (params.size() != variables.size()) {
		throw std::runtime_error("Parameter mismatch");
	}
	s.functions.push_back(std::map<std::string, Function>());

	for (unsigned int i = 0; i < variables.size(); i++) {
		Function f;
		f.name = variables[i];
		f.seq = params[i];
		s.functions.back().insert(std::pair<std::string, Function>(f.name, f));
	}

	s.functions.pop_back();
}

