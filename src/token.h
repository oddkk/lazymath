#ifndef TOKEN_H
#define TOKEN_H
#include <string>
#include <memory>
#include <vector>

enum class TokenType {
	word,
	number,
	symbol,
};

enum class Operator {
	none,
	plus,
	minus,
	multiply,
	divide,
	power,
	equals,
	parenthesis_open,
	parenthesis_close,
	squarebracket_open,
	squarebracket_close,
};

Operator operatorFromSymbol(std::string sym);
std::string symbolFromOperator(Operator op);

class Token {
public:
	virtual const TokenType type() const=0;
	virtual const std::string to_string() const=0;
};

class WordToken {
public:
	WordToken(std::string value="") : value(value) {}
	virtual inline const TokenType type() const { return TokenType::word; }
	virtual inline const std::string to_string() const { return value; }
	std::string value;
};

class NumberToken {
public:
	NumberToken(double value=0.0) : value(value) {}
	virtual inline const TokenType type() const { return TokenType::number; }
	virtual inline const std::string to_string() const { return std::to_string(value); }
	double value;
};

class SymbolToken {
public:
	SymbolToken(Operator value=Operator::none): value(value) {}
	virtual inline const TokenType type() const { return TokenType::symbol; }
	virtual inline const std::string to_string() const { return symbolFromOperator(value); }
	Operator value;
};

class TokenList
{
public:
	std::vector<std::unique_ptr<Token>> tokens;

	void reset();
	void next();

	Token* token();
	TokenType type();
	bool isWord();
	bool isNumber();
	bool isSymbol();

	std::string getWord();
	double getNumber();
	Operator getSymbol();

	bool eof();

	void assertFail(std::string val="");
	void assertWord();
	void assertWord(std::string);
	void assertNumber();
	void assertSymbol();
	void assertSymbol(Operator);
	void assertNotEOF();
private:
	unsigned int _i;
};

#endif
