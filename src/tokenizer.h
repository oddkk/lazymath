#ifndef TOKENIZER_H
#define TOKENIZER_H
#include <string>
#include "token.h"

class Tokenizer
{
public:
	static void tokenize(std::string str, TokenList& out);
};

#endif

