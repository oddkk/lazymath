#include "tokenizer.h"
#include <iostream>
#include <stdexcept>
#include <cctype>

static bool isNumber(int c) {
	return isdigit(c) || c == '.';
}

static bool isAlphabetic(int c) {
	return isalpha(c);
}

static bool isSymbol(int c) {
	return ispunct(c);
}

static std::string getWhile(std::string::iterator& it, const std::string::iterator& end, bool(*cond)(int)) {
	std::string res = "";
	while (it != end && cond(*it)) {
		res += *it++;
	}
	return res;
}

static Operator getSymbol(std::string::iterator& it, const std::string::iterator& end) {
	std::string sym = "";
	Operator op = Operator::none;
	while (operatorFromSymbol(sym + *it) != Operator::none) {
		sym += *it++;
		op  = operatorFromSymbol(sym);
	}
	return op;
}

void Tokenizer::tokenize(std::string str, TokenList& out) {
	out.reset();
	auto it = str.begin();
	while (it != str.end()) {
		if (isNumber(*it)) {
			std::string word = getWhile(it, str.end(), isNumber);
			out.tokens.push_back(std::unique_ptr<Token>((Token*)new NumberToken(atof(word.c_str()))));
		}
		else if (isAlphabetic(*it)) {
			std::string word = getWhile(it, str.end(), isAlphabetic);
			out.tokens.push_back(std::unique_ptr<Token>((Token*)new WordToken(word)));
		}
		else if (isSymbol(*it)) {
			Operator op = getSymbol(it, str.end());
			out.tokens.push_back(std::unique_ptr<Token>((Token*)new SymbolToken(op)));
			if (op == Operator::none) it++;
		}
		else {
			it++;
		}
	}
}

