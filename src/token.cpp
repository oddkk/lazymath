#include "token.h"
#include <stdexcept>

static struct {
	Operator op;
	std::string symbol;
} _operators[] = {
	{ Operator::none, "(none)" },
	{ Operator::plus, "+" },
	{ Operator::minus, "-" },
	{ Operator::multiply, "*" },
	{ Operator::divide, "/" },
	{ Operator::power, "^" },
	{ Operator::equals, "=" },
	{ Operator::parenthesis_open, "(" },
	{ Operator::parenthesis_close, ")" },
	{ Operator::squarebracket_open, "[" },
	{ Operator::squarebracket_close, "]" },
};

Operator operatorFromSymbol(std::string sym) {
	for (unsigned int i = 0; i < sizeof(_operators) / sizeof(_operators[0]); i++) {
		if (_operators[i].symbol == sym) return _operators[i].op;
	}
	return Operator::none;
}

std::string symbolFromOperator(Operator op) {
	for (unsigned int i = 0; i < sizeof(_operators) / sizeof(_operators[0]); i++) {
		if (_operators[i].op == op) return _operators[i].symbol;
	}
	return "(none)";
}

void TokenList::reset() {
	_i = 0;
}

void TokenList::next() {
	_i++;
}

Token* TokenList::token() {
	return tokens[_i].get();
}

TokenType TokenList::type() {
	return tokens[_i]->type();
}

bool TokenList::isWord() {
	return type() == TokenType::word;
}

bool TokenList::isNumber() {
	return type() == TokenType::number;
}

bool TokenList::isSymbol() {
	return type() == TokenType::symbol;
}

std::string TokenList::getWord() {
	assertWord();
	return ((WordToken*)tokens[_i].get())->value;
}

double TokenList::getNumber() {
	assertNumber();
	return ((NumberToken*)tokens[_i].get())->value;
}

Operator TokenList::getSymbol() {
	assertSymbol();
	return ((SymbolToken*)tokens[_i].get())->value;
}

bool TokenList::eof() {
	return _i >= tokens.size();
}

static std::string tokenTypeName(TokenType tt) {
	switch (tt) {
	case TokenType::word: return "word";
	case TokenType::number: return "number";
	case TokenType::symbol: return "symbol";
	}
}

void TokenList::assertFail(std::string val) {
	throw std::runtime_error("Assertion failed: " + val);
}

void TokenList::assertWord() {
	if (type() != TokenType::word)
		assertFail("token " + tokenTypeName(type()) + " != " + tokenTypeName(TokenType::word));
}

void TokenList::assertWord(std::string val) {
	assertWord();
	if (getWord() != val)
		assertFail("Word '" + getWord() + "' != '" + val + "'");
}

void TokenList::assertNumber() {
	if (type() != TokenType::word)
		assertFail("token " + tokenTypeName(type()) + " != " + tokenTypeName(TokenType::number));
}

void TokenList::assertSymbol() {
	if (type() != TokenType::word)
		assertFail("token " + tokenTypeName(type()) + " != " + tokenTypeName(TokenType::symbol));
}

void TokenList::assertSymbol(Operator val) {
	assertSymbol();
	if (getSymbol() != val)
		assertFail("Symbol '" + symbolFromOperator(getSymbol()) + "' != '" + symbolFromOperator(val) + "'");
}

void TokenList::assertNotEOF() {
	if (eof())
		assertFail("Unexpected end of file");
}
