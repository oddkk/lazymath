CC=clang++

CFLAGS=-c -g -Wall -std=c++11 -D _DEBUG $(shell freetype-config --cflags)
SOURCEDIR=./src/
OUTDIR=./build/
SOURCES=$(shell find . -name *.cpp)
OBJECTS=$(SOURCES:$(SOURCEDIR)%.cpp=$(OUTDIR)%.o)
OUTPUT=$(OUTDIR)lazymath

all: $(OUTPUT)

.PHONY: all

clear:
	rm -rf $(OUTDIR)

.PHONY: clear

rebuild: clear $(OUTPUT)  $(OUTDIR)assets/ $(OUTDIR)save/

.PHONY: rebuild

relink: rmoutput $(OUTPUT)

.PHONY: relink

rmoutput:
	rm $(OUTPUT)

.PHONY: rmoutput

$(OUTPUT): $(OBJECTS)
	$(CC) $(OBJECTS) -std=c++11 -o $(OUTPUT)

$(OUTDIR)%.o:$(SOURCEDIR)%.cpp
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $< -o $@
